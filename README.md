# Learn React with SWR

## Installation

- Json-server: `npm install -g json-server`
- Run server (json-server): `npx json-server -w data/db.json -p 3500`

## Sources

- [Youtube video](https://youtu.be/6gb6oyO1Tyg)
- [Github repository for source code](https://github.com/gitdagray/react-swr)
- [SWR documentation](https://swr.vercel.app/)
